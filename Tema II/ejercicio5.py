# Programa Quinto
'''
    Programa que calcula el área de una circunferencia
'''

from math import pi # Cargar la constante pi del modulo math

radio=float(input("Proporcione el radio de la circunferencia:"))

area=pi*radio**2

print ("El area del circulo con radio:",radio," es =",area)


